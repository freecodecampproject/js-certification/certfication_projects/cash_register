let price = 19.5
let cid = [
  ["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]
]

// --- start Here

const displayChangeDue = document.querySelector('#change-due')
const cash = document.querySelector('#cash')
const purchaseBtn = document.querySelector('#purchase-btn')
const priceScreen = document.querySelector('#price-screen')
const cashDrawerDisplay = document.querySelector('#cash-drawer-display')


const formatResults = (status, change) => {
  displayChangeDue.innerHTML = `<p>Status: ${status}</p>`
  change.map(money => displayChangeDue.innerHTML += `<p>${money[0]}: $${money[1]}</p>`)
}

const checkCashRegister = () => {
  if(Number(cash.value) < price) {
    alert('Customer does not have enough money to purchase the item')
    cash.value = ''
    return
  }

  if (Number(cash.value) === price) {
    displayChangeDue.innerHTML = "<p>No change due - customer paid with exact cash</p>"
    cash.value = ''
    return
  }

  let changeDue = Number(cash.value) - price
  let reverseCid = [...cid].reverse()
  let denominations = [100, 20, 10, 5, 1, 0.25, 0.1, 0.05, 0.01]
  let result = {status: 'OPEN', change: []}
  let totalCID = parseFloat(
      cid
          .map(total => total[1])
          .reduce((acc, cur) => acc + cur , 0)
          .toFixed(2)
  )

  if(totalCID < changeDue) {
    return displayChangeDue.innerHTML = "<p>Status: INSUFFICIENT_FUNDS</p>"
  }

  if(totalCID === changeDue) {
    result.status = 'CLOSED'
  }

  for (let i = 0; i <= reverseCid.length; i++) {
    if(changeDue > denominations[i] && changeDue > 0) {
      let count = 0
      let total = reverseCid[i][1]
      while (total > 0 && changeDue >= denominations[i]) {
        total -= denominations[i]
        changeDue = parseFloat((changeDue -= denominations[i]).toFixed(2))
        count++
      }
      if(count > 0) {
        result.change.push([reverseCid[i][0], count * denominations[i]])
      }
    }
  }

  console.log(changeDue)
  if(changeDue > 0) {
    return displayChangeDue.innerHTML = "<p>Status: INSUFFICIENT_FUNDS</p>"
  }

  formatResults(result.status, result.change)
  updateUi(result.change)
}

const checkResults = () => {
  if(!cash.value) {
    return
  }

  checkCashRegister()
}


const updateUi = change => {
  const currencyNameMap = {
    PENNY: "Pennies",
    NICKEL: "Nickels",
    DIME: "Dimes",
    QUARTER: "Quarters",
    ONE: "Ones",
    FIVE: "Fives",
    TEN: "Tens",
    TWENTY: "Twenties",
    "ONE HUNDRED": "Hundreds",
  }

  if (change) {
    change.forEach(changeArr => {
      const targetArr = cid.find((cidArr) => cidArr[0] === changeArr[0])
      targetArr[1] = parseFloat((targetArr[1] - changeArr[1]).toFixed(2))
    })
  }

  cash.value = ''
  priceScreen.textContent = `Total: $${price}`
  cashDrawerDisplay.innerHTML = `
    <p><strong>Change in drawer:</strong></p>
    ${
    cid.map(money => `<p>${currencyNameMap[money[0]]}: $${money[1]}</p>`).join('')
    }
  `
}

purchaseBtn.addEventListener('click', checkResults)

cash.addEventListener('keydown', ({key}) => {
  if(key === 'Enter') {
    checkResults()
  }
})

updateUi()